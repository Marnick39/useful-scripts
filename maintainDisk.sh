#!/bin/bash
CMD="df -h --output=avail /dev/sdb2 | grep ' '| sed 's/ //g'"
PRE=$(eval "$CMD")
echo "Verifying journals.."
sudo -user $user journalctl --verify  &> /dev/null
echo "Trimming journals.."
sudo -user $USER journalctl --vacuum-size=100M  &> /dev/null
echo "Removing old packages.."
pacman -Sc --noconfirm  1> /dev/null
POST=$(eval "$CMD")
echo "Went from $PRE available disk space to $POST."

