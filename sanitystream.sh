#!/bin/bash

# URL prep
URL='"'
URL+=$1
URL+='"'

# Command assembly
CMD='rm /tmp/mediafile.mp3; youtube-dl --output "/tmp/mediafile.%(ext)s" --extract-audio --audio-format mp3 --audio-quality 0 '
MOVE=';cp /tmp/mediafile.mp3 <SuperSecretLocationOnServer>'

CMD+=$URL
CMD+=$MOVE

# Execute on lapseofsanity.net
ssh lapseofsanity.net -C $CMD

