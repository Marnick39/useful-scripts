#!/bin/bash

# ---------------------------------------------------------------------------------
#  cgrep aims to prints context along with what it finds in a nicely formatted way
#     (much like 'grep -rn -C 5')
#   
#     * Features *
#   - Support for binary files
#   - Live feedback of found files (before context is supplied)
#  
#     * TO DO *
#   - Support for difficult files (with spaces)
#
# ---------------------------------------------------------------------------------

# Colors for output
GRAY='\033[1;30m'
RED='\033[0;31m'
BLUE='\033[1;34m'
NC='\033[0m'

# Check parameter count, if not all required parameters are there print instruction on how to use cgrep
if (( $# < 1 )); then
    echo "Provide a string to look for:"
    echo -e "./cgrep.sh <string_to_look_for> <directory_to_look_through>${GRAY}*${NC} <lines_of_context_to_provide>${GRAY}*${NC}"
    echo -e "${GRAY}*optional parameters${NC}"
    exit -1
fi

# Get target string the user is looking from the first parameter
TARGET=$1

# Get target dir the user is looking through from the second parameter (optional)
DIR=$2
if [  -z "$2" ]
  then 
    DIR=$(pwd)
fi
# Get number of lines of context to print from the third parameter (optional)
LINES=$3
if [  -z "$3" ]
  then 
    LINES=5
fi

# Collect all files that contain the string whilst removing all errors (no more "Permission Denied"!) 
#   and printing our find live 
echo "Searching for files that contain '$TARGET' in $DIR"
FILE_HITS=$(grep -irl $TARGET $DIR 2>/dev/null | tee /dev/tty)

# Print findings
for i in $FILE_HITS;
do 
        # Put brackets around the target, to be compatible  with spaces in file names
        echo -e "${GRAY}~~~~~~~~~~~~~~~~~~~~~~{${BLUE}$i${GRAY}}${NC}:"
        # Run 'strings $i' before grepping for quick & good compatibility with binary files
        echo -e "$(strings $i | grep --color=always -C $LINES -n $TARGET) \n"
done
