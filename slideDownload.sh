#!/bin/bash

# for loop from 1/10 to 10/10
for a in `seq 1 21`; do
    wget -nv "<URLofterribleslideshow>-$a-1024.jpg"
    #sleep 1;
done
echo "Completed download, generating pdf.." 
/bin/convert $(ls -1 *.jpg | sort -V) presentation.pdf
echo "Cleaning up jpgs."
rm *.jpg
echo "DONE!"

